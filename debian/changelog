libcdk-perl (20240606-1) unstable; urgency=medium

  * Import upstream version 20240606.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 4.7.0.

 -- gregor herrmann <gregoa@debian.org>  Sun, 28 Jul 2024 23:30:12 +0200

libcdk-perl (20230205-1) unstable; urgency=medium

  * Import upstream version 20230205.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Fri, 10 Feb 2023 21:12:01 +0100

libcdk-perl (20221025-1) unstable; urgency=medium

  * Team upload

  * Import upstream version 20221025.
  * Document upstream license change (BSD-4-clause to X11)
  * Update lintian-overrides format

 -- Florian Schlichting <fsfs@debian.org>  Sat, 26 Nov 2022 20:13:06 +0100

libcdk-perl (20211216-1) unstable; urgency=medium

  * Update debian/watch to use HTTPS instead of FTP, and add pgpmode=auto.
    Also add upstream key to debian/upstream/signing-key.asc.

  * Import upstream version 20211216.
  * Pass --without-cdk --without-cdkw to ./configure.
    Usage of cdk(w)5-config is now the default.
  * Update years of upstream and packaging copyright.
  * Enable all hardening flags.
  * Use HTTPS for Source field in debian/copyright.
  * Declare compliance with Debian Policy 4.6.1.
  * Set Rules-Requires-Root: no.
  * Bump debhelper-compat to 13.
  * Add a pedantic lintian override.
  * Fix typo in previous changelog entry.

 -- gregor herrmann <gregoa@debian.org>  Sat, 11 Jun 2022 16:28:34 +0200

libcdk-perl (20150928-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jeremiah Foster from Uploaders. Thanks for your work!
    Closes: #843735

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * Declare compliance with Debian Policy 4.4.0.
  * Update alternative build dependency.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.
  * Use secure URI in Homepage field.
  * Drop override_dh_compress.
    debhelper in compat level 12 doesn't compress examples anymore.
  * Remove autotools debhelper addon.
    Not supported by debhelper 12 anymore and done automatically.
  * debian/control: update Build-Depends for cross builds.

  [ Debian Janitor ]
  * Update standards version to 4.4.1, no changes needed.

  [ Andrius Merkys ]
  * Update debian/watch.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 09 Jun 2022 22:54:14 +0100

libcdk-perl (20150928-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * New upstream release.
  * Update years of upstream and packaging copyright.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Sat, 05 Dec 2015 21:03:52 +0100

libcdk-perl (20130816-1) unstable; urgency=low

  * New upstream release.
  * Drop XS syntax patch, merged upstream.

 -- gregor herrmann <gregoa@debian.org>  Sat, 21 Sep 2013 22:17:58 +0200

libcdk-perl (20130717-2) unstable; urgency=low

  * Team upload.
  * Fix XS syntax, enforced by newer versions of ExtUtils::ParseXS.
    (Closes: #719578)

 -- Niko Tyni <ntyni@debian.org>  Thu, 15 Aug 2013 22:27:32 +0300

libcdk-perl (20130717-1) unstable; urgency=low

  * New upstream release.
  * Update years of upstream copyright.
  * Drop patch, merged upstream.

 -- gregor herrmann <gregoa@debian.org>  Fri, 09 Aug 2013 21:41:49 +0200

libcdk-perl (20120324-2) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)

  [ gregor herrmann ]
  * Add patch from Niko Tyni to fix a few parameter typos caught be newer
    versions of ExtUtils::ParseXS. (Closes: #708593)
  * Update years of packaging copyright.
  * Set Standards-Version to 3.9.4 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Sat, 18 May 2013 13:51:34 +0200

libcdk-perl (20120324-1) unstable; urgency=low

  * debian/control: update {versioned,alternative} (build) dependencies.

  * New upstream release.
  * Patches:
    - drop cdk_viewer.patch, merged upstream
    - drop patch cdkdemo_help.patch, merged upstream
    - drop makefile.patch, hppa-specific
  * debian/rules:
    - call dh_auto_configure twice:
      once for ./configure, and then again for Makefile.PL. Otherwise the perl
      modules get installed into the wrong directory, since INSTALLDIRS is not
      passed along
    - add empty override_dh_auto_test; there are no tests
  * Install examples files with examples sub-directory. Add a lintian
    override for the examples/examples dir.
  * debian/{contro,compat,rules}:
    - use debhelper 9.20120312 to get all hardening flags
    - bump Standards-Version to 3.9.3 (no changes)
    - use autotools-dev to ensure recent versions of config{.guess,sub}
  * debian/copyright:
    - update to Copyright-Format 1.0
    - update years of upstream and packaging copyright and license stanzas
  * Add files to debian/clean that are created during build and not removed by
    the upstream build system.

 -- gregor herrmann <gregoa@debian.org>  Sun, 26 Aug 2012 19:00:56 +0200

libcdk-perl (4.9.10-5) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Dominic Hargreaves ]
  * Add Depends on libperl4-corelibs-perl (will fix lintian warning
    perl-module-uses-perl4-libs-without-dep once #648532 has been fixed)
  * Update Standards-Version (no changes)
  * Switch to dpkg-source 3.0 (quilt) format, restoring the application
    of patches lost in 4.9.10-4

 -- Dominic Hargreaves <dom@earth.li>  Sat, 12 Nov 2011 16:43:21 +0000

libcdk-perl (4.9.10-4) unstable; urgency=low

  * Move changes to upstream code under debian/patches; add quilt framework.
  * Add debian/README.source to document quilt usage, as required by
    Debian Policy since 3.8.0.
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * debian/control: Added: ${misc:Depends} to Depends: field.
  * New patch cdkdemo_help.patch to change default path for help system in
    cdkdemo; thanks to Rafael Laboissiere for the bug report (closes: #519820).
  * Don't install README anymore.
  * Switch to debhelper 7, adjust debian/{rules,compat,control}.
  * Set Standards-Version to 3.8.2 (no further changes).
  * Add /me to Uploaders.
  * debian/copyright: switch to new format.

 -- gregor herrmann <gregoa@debian.org>  Sun, 09 Aug 2009 16:36:30 +0200

libcdk-perl (4.9.10-3) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed:
    Homepage pseudo-field (Description); XS-Vcs-Svn fields.
  * debian/rules: delete /usr/share/perl5 only if it exists.

  [ Damyan Ivanov ]
  * Standards-Version: 3.7.3 (no changes)
  * debhelper compatibility level 6
  * debian/rules:
    + make it like the current dh-make-perl templates
  * add myself to Uploaders

 -- Damyan Ivanov <dmn@debian.org>  Mon, 21 Jan 2008 11:41:29 +0200

libcdk-perl (4.9.10-2) unstable; urgency=low

  * Adopted orphaned package for Debian Perl Group (Closes: #279778)
  * Added myself to Uploaders
  * Do not ignore $(MAKE) clean errors in debian/rules
  * Corrected Homepage in debian/control
  * Removed emtpy /usr/share/perl5
  * Fixed debian/watch
  * Bumped debhelper version in debian/compat to 5
  * Changed >> to >= in Build-Depends
  * Updated Standards-Version to 3.7.2 from 3.6.2 (no changes)

 -- Jeremiah Foster <jeremiah@jeremiahfoster.com>  Tue, 14 Aug 2007 12:39:04 +0000

libcdk-perl (4.9.10-1) unstable; urgency=low

  * QA upload.
  * Package is orphaned (see #279778); set maintainer to Debian QA Group.
  * New upstream release.
    - Links against libcdk5 rather than libcdk4.
    - Fixes the matrix widget.  Closes: #179540.
  * Cdk/Viewer.pm: Fix copy-paste error that prevented bind() from working.
    Thanks to Mathew White for the patch.  Closes: #312733.
  * Change section to perl in accordance with the override file.
  * Update description.  Closes: #205877.
  * debian/copyright:
    - Include original copyright and license.  Closes: #236488.
    - Update upstream URL.
  * debian/rules: Leave examples uncompressed.
  * debian/watch: Add.
  * Conforms to Standards version 3.6.2.

 -- Matej Vela <vela@debian.org>  Mon, 31 Oct 2005 13:09:31 +0100

libcdk-perl (4.9.7-6) unstable; urgency=low

  * Redo the hppa specific inclusion of -ffunction-sections as
    $Config::Config{archname} has changed.

 -- Stephen Zander <gibreel@debian.org>  Tue,  5 Nov 2002 01:37:34 -0800

libcdk-perl (4.9.7-5) unstable; urgency=low

  * Rebuild for perl 5.8; adjust Build-Depends appropriately

 -- Stephen Zander <gibreel@debian.org>  Tue,  3 Sep 2002 07:32:33 -0700

libcdk-perl (4.9.7-4) unstable; urgency=low

  * Add -ffunction-sections for hppa architecture,
  Closes: #134027

 -- Stephen Zander <gibreel@debian.org>  Fri, 17 May 2002 16:23:35 -0700

libcdk-perl (4.9.7-3) unstable; urgency=high

  * Fix Section: override.
  * Set urgency=high to ensure package moves to testing more quickly

 -- Stephen Zander <gibreel@debian.org>  Fri, 28 Dec 2001 12:43:36 -0800

libcdk-perl (4.9.7-2) unstable; urgency=low

  * New maintainer,
  Closes: #123489
  * Updated for policy 3.5.6 and the new perl packaging policy,
  Closes: #80683, #95406
  * Builds cleanly on sparc & hppa,
  Closes: #89292, #104886
  * No longer build libcdk-perl-examples,
  Closes: #118225

 -- Stephen Zander <gibreel@debian.org>  Fri, 28 Dec 2001 09:57:48 -0800

libcdk-perl (4.9.7-1) unstable; urgency=low

  * New upstream version.

 -- Raphael Bossek <bossekr@debian.org>  Sat, 15 Jan 2000 12:51:09 +0100
